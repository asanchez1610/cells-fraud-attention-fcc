import { LitElement, html, } from 'lit-element';
import { getComponentSharedStyles } from '@bbva-web-components/bbva-core-lit-helpers';
import {themeVoceFCC} from '@cells-voce-components/cells-voce-theme-fcc/cells-voce-theme-fcc.js';
import '@cells-voce-components/cells-voce-utils-behavior-fcc/cells-voce-utils-behavior-fcc.js';
import '@bbva-web-components/bbva-button-default';
import '@bbva-web-components/bbva-form-date';
import '@cells-voce-components/cells-modal-fcc/cells-modal-fcc.js';
import '@bbva-web-components/bbva-form-checkbox/bbva-form-checkbox.js';
const utilsBehaviorFcc = CellsBehaviors.CellsVoceUtilsBehaviorFcc;
import styles from './CellsFraudAttentionFcc-styles.js';
import '@bbva-web-components/bbva-web-form-select/bbva-web-form-select.js';
/**
![LitElement component](https://img.shields.io/badge/litElement-component-blue.svg)

This component ...

Example:

```html
<cells-fraud-attention-fcc></cells-fraud-attention-fcc>
```

##styling-doc

@customElement cells-fraud-attention-fcc
*/
export class CellsFraudAttentionFcc extends utilsBehaviorFcc(LitElement) {
  static get is() {
    return 'cells-fraud-attention-fcc';
  }

   // Declare properties
   static get properties() {
    return {
      visible: { type: Boolean},
      title: { type: String},
      evtData: {type: Object},
      dateFrom: {type: String},
      dateTo: {type: String},
      transactionsList: {type: Array},
      visibleDates: {type: Boolean},
      visibleList: {type:Boolean},
      visibleButton: {type:Boolean},
      view: {type: String},
      contract: {type: String},
      image: { type: String},
      dataModal: {type: Object},
      dataSuccess: {type:Object},
      status: {type: String},
      dataOperation: {type: Object},
      enableAccepted: {type:Boolean},
      itemsMonth: { type: Array },
      selectionsMovements: { type: Array },
      simulationOk: { type: Boolean }
    };
  }

  // Initialize properties
  constructor() {
    super();
    this.visible = false;
    this.title = 'OPERACIONES NO RECONOCIDAS';
    this.evtData = {};
    this.dateFrom = '';
    this.dateTo = '';
    this.transactionsList = [];
    this.visibleDates = false;
    this.visibleList = false;
    this.visibleButton = false;
    this.view = '';
    this.contract = '';
    this.image = '';
    this.dataModal = {};
    this.dataSuccess = {};
    this.status = '';
    this.dataOperation = {};
    this.enableAccepted = false;
    this.itemsMonth = this._generateMonths();
    this.selectionsMovements = [];
    this.simulationOk = false;
  }

  static get shadyStyles() {
    return `
      ${styles.cssText}
      ${themeVoceFCC}
      ${getComponentSharedStyles('cells-account-fraud-attention-fcc-shared-styles').cssText}
    `;
  }

  setVisibleDivs() {
    this.visibleDates = true;
    this.visibleList = true;
    this.visibleButton = true;
  }
  
  initFraudAttention(evt) {
    this.clearSelections();
    this.setVisibleDivs();
    this.setListfraudAttention(evt)
  }

  _generateMonths() {
    const date = new Date();
    const month = date.getMonth() + 1;
    const year = date.getFullYear();
    const array = [];
    let aux = 12;
    let value = '';
    for (let i = 0; i < 6; i++) {
      const rest = month - i;
      if (rest <= 0) {
        value = (`${year - 1}-${this.getMonthIndex(`${aux}`)}`);
        array.push({ value, text: this.getNameMonth(value) });
        aux--;
      } else if (i == 0) {
        value = `${year}-${this.getMonthIndex(`${rest}`)}`;
        array.push({ value, text: this.getNameMonth(value), selected: true });
      } else {
        value = `${year}-${this.getMonthIndex(`${rest}`)}`;
        array.push({ value, text: this.getNameMonth(value) });
      }
    }
    return array;
  }

  getMonthIndex(month) {
    const strMonth = (month.length == 2 ? month : `0${month}`);
    return strMonth;
  }

  getNameMonth(month) {
    const strMonth = month.split('-')[1];
    let result = '';
    switch (strMonth) {
      case '01': result = 'Enero'; break;
      case '02': result = 'Febrero'; break;
      case '03': result = 'Marzo'; break;
      case '04': result = 'Abril'; break;
      case '05': result = 'Mayo'; break;
      case '06': result = 'Junio'; break;
      case '07': result = 'Julio'; break;
      case '08': result = 'Agosto'; break;
      case '09': result = 'Setiembre'; break;
      case '10': result = 'Octubre'; break;
      case '11': result = 'Noviembre'; break;
      case '12': result = 'Diciembre'; break;
      default: result = ''; break;
    }
    return result;
  }

  shortMonth(month) {
    let result = '';
    switch (month) {
      case '01': result = 'ENE'; break;
      case '02': result = 'FEB'; break;
      case '03': result = 'MAR'; break;
      case '04': result = 'ABR'; break;
      case '05': result = 'MAY'; break;
      case '06': result = 'JUN'; break;
      case '07': result = 'JUL'; break;
      case '08': result = 'AGO'; break;
      case '09': result = 'SET'; break;
      case '10': result = 'OCT'; break;
      case '11': result = 'NOV'; break;
      case '12': result = 'DIC'; break;
      default: result = ''; break;
    }
    return result;
  }

  setListfraudAttention(evt) {
    this.transactionsList = [];
    this.visible = true;
    this.evtData = evt.evtData;
    let data = evt;
    this.contract = this.extract(evt, 'evtData.key.cta', '');
    let image = this.extract(evt, 'image', '');
    if (data.fraudAttention !== null && data.fraudAttention !== undefined) {
      data.fraudAttention.forEach((el) => {
        if (el.amount.value < 0) {
          this.transactionsList.push(el);
        }
      });
    }
    this.view = '1';
    this.image = this.getImage(image);
    this.setFilterDates();
  }

  setAmountFormat(amount) {
    let amountValue = amount.value + '';
    let currency = this.formatCurrency(amount.currency);
    let resp = '';
    if (!this.isEmpty(amount)) {
      let minus = amountValue.substring(0, 1);
      let number = this.formatCurrencyAmount(currency, amountValue);
      let float = amountValue.split('.')[1];
      let decimal = '';
      let negative = '';
      if (minus === '-') {
        negative = 'negative';
      } else {
        negative = '';
      }
      if (typeof float === 'undefined') {
        decimal = '00';
      } else {
        if (float.length === 1) {
          decimal = float + '0';
        } else {
          decimal = float;
        }
      }
      this.amountData = {
        'number': number,
        'decimal': decimal,
        'negative': negative
      };
      resp = number + '.' + decimal;
    } else {
      resp = '';
    }
    return resp;
  }

  formatCurrencyAmount(currency, amount) {
    let resp = '';
    let integerNumber = '';
    let minus = '';
    let numberArray = amount.split('-');
    if (numberArray.length > 1) {
      minus = amount.substring(0, 1);
      integerNumber = numberArray[1].split('.')[0];
      resp = minus + currency + this.formatNumber(integerNumber);
    } else {
      minus = '';
      integerNumber = amount.split('.')[0];
      resp = minus + currency + this.formatNumber(integerNumber);
    }
    return resp;
  }

  formatNumber (number) {
    let reg=/\d{1,3}(?=(\d{3})+$)/g; 
		return (number + '').replace(reg, '$&,');
  }

  setFilterDates() {
    this.dateFrom = '';
    this.dateTo = '';
    this.dateFrom = this.dateFormat(this.substractDate(1, 'M'), 'YYYY-MM-DD');
    this.dateTo = this.dateFormat(new Date(), 'YYYY-MM-DD');
  }

  setDateFomat(date) {
    let resp = '';
    if (!this.isEmpty(date)) {
      let year = date.split('-')[0];
      let month = date.split('-')[1];
      let dayExtract = date.split('-')[2];
      let day = dayExtract.split('T')[0];
      this.dateData = {
        'day': day,
        'month': this.shortMonth(month),
        'year': year
      };
      resp = date;
    } else {
      resp = '';
    }
    return resp;
  }

  shortMonth(month) {
    let result = '';
    if (!this.isEmpty(month)) {
      let arrayValues = [
        {'id': '01', 'month': 'ENE'},
        {'id': '02', 'month': 'FEB'},
        {'id': '03', 'month': 'MAR'},
        {'id': '04', 'month': 'ABR'},
        {'id': '05', 'month': 'MAY'},
        {'id': '06', 'month': 'JUNI'},
        {'id': '07', 'month': 'JUL'},
        {'id': '08', 'month': 'AGO'},
        {'id': '09', 'month': 'SET'},
        {'id': '10', 'month': 'OCT'},
        {'id': '11', 'month': 'NOV'},
        {'id': '12', 'month': 'DIC'},
      ];
      let data = arrayValues.find(item => item.id === month);
      result = data.month;
    } else {
      result = '';
    }
    return result;
  }

  clickChkDocument(evt) {
    let checkboxes = this.shadowRoot.querySelectorAll('.choiceChk');
    let selections = [];
    checkboxes.forEach(chk => {
      const item = JSON.parse(chk.dataset.item);
      if (chk.checked) {
        item.amountValue = this.setAmountFormat(item.amount);
        selections.push(item);
      }
    });
    this.selectionsMovements = selections;
    console.log(selections);
    this.enableAccepted = selections.length > 0;
  }

  resetCheckBoxes(checkboxes) {
    let lista = checkboxes;
    for (let i = 0; i < lista.length; i++) {
      lista[i].checked = false;
    }
  }

  getIndexComposedPath(path, name) {
    let resp = 0;
    let data = path;
    if (!this.isEmpty(data)) {
      for (let a = 0; a < data.length; a++) {
        if (data[a].id !== undefined) {
          if (data[a].id.indexOf('' + name + '') === 0) {
            resp = a;
          }
        }
      }
    }
    return resp;
  }

  findTransactions() {
    console.log('findTransactions');
    let dInitial =  this.shadowRoot.querySelector('#dtFechaDesde');
    let dFinal =  this.shadowRoot.querySelector('#dtFechaHasta');
    if (!this.isEmpty(dInitial.value) && !this.isEmpty(dFinal.value)) {
      if (dInitial.value !== "dd-mm-aaaa" && dFinal.value !== "dd-mm-aaaa") {
        if (this.isBefore(dInitial.value, dFinal.value)) {
          this.error({
            message: 'Error en los filtros: La fecha final debe ser mayor a la fecha inicial',
            duration: '10',
            type: 'error'
          });
        } else {
          this.evtData.key.dateFrom = dInitial.value;
          this.evtData.key.dateTo = dFinal.value;
          this.clearSelections();
          this.dispatch('fraud-attention-search-dates', this.evtData);
        }
      }
    }
  }

  acceptTransfer(evt) {
    if (this.selectionsMovements.length > 0) {
      this.shadowRoot.querySelector('#modalOriginAccount').open();
    }
  }

  confirmTransfer() {
    if (this.simulationOk) {
      this.dispatch('on-aplication-attention-refund', this.selectionsMovements);
    } else {
      this.dispatch('on-simulate-attention-refund', this.selectionsMovements);
    } 
  }

  cancelConfirm() {
    this.shadowRoot.querySelector('#modalOriginAccount').close();
    this.shadowRoot.querySelectorAll('.content-item-selection').forEach(item => {
      item.classList.remove('success-movement');
      item.classList.remove('error-movement');
    });
    this.view = '1';
    this.simulationOk = false;
  }

  showModalAfterConfirm(evt) {
    console.log('showModalAfterConfirm', evt);
    this.status = this.extract(evt, 'status', '');
    if (this.status === 'ok-simulation') {
      this.view = '1';
      this.simulationOk = true;
      //this.shadowRoot.querySelector('#btn-confirmar').innerHTML = 'Abonar';
      this.shadowRoot.querySelectorAll('.content-item-selection').forEach(item => {
        item.classList.add('success-movement')
      });
    } else if (this.status === 'ok-application') {
      this.shadowRoot.querySelector('#modalOriginAccount').close();
      this.view = '2';
      this.dataModal = {
        icon: 'correct',
        message: 'La devolución de la operación se ha realizado con éxito.',
        iconToClient: 'incomingcall',
        messageToClient: '"Sr/ Srta, le informamos que la devolución por fraude ha sido realizada correctamente.'
      };

      let currency = this.extract(evt, 'dataResponse.data.firstContactResolution.sentMoney.currency', '');
      currency = this.formatCurrency(currency);
      this.dataOperation = {
        number:  this.extract(evt, 'dataResponse.data.relatedClaim.id', '--'),
        dateHour: this.extract(evt, 'dateOperationResponse', '--'),
        sendingProof: this.extract(evt, 'email', '--'),
      };
      this.dataSuccess = {
        icon: 'alert',
        message: 'Recuerda solicitar la conformidad al cliente al final de la atención. Si el cliente se encuentra conforme se concluirá la atención de la misma de lo contrario, se deberá ingresar el reclamo.',
        iconToClient: 'incomingcall',
        messageToClient: '"¿Esta conforme con la atención brindada o desea ingresar un reclamo?"'
      };

      this.shadowRoot.querySelector('#modalTransferReversal').open();
    } else {
      //this.shadowRoot.querySelector('#modalOriginAccount').close();
      this.view = '1';
      this.simulationOk = false;
      this.shadowRoot.querySelectorAll('.content-item-selection').forEach(item => {
        item.classList.add('error-movement')
      });
      this.dataModal = {
        icon: 'alert',
        message: 'No aplica flujo de devolución por fraude desde el frontal.',
        iconToClient: 'incomingcall',
        messageToClient: '"Sr/ Srta, vamos a ingresar una solicitud para que un analista pueda revisar su caso y poder brindarle una solución."'
      }
      this.shadowRoot.querySelector('#modalTransferReversal').open();
    }
  }

  getImage(image) {
    let resp = '';
    if (image === '') {
      resp = './images/headset.png';
    } else {
      resp = image;
    }
    return resp;
  }

  continueTransfer(evt) {
    this.shadowRoot.querySelector('#modalTransferReversal').close();
  }

  formatCurrency(code) {
    let simbol = '';
    switch (code) {
      case 'PEN': {
        simbol = 'S/';
        break;
      }
      case 'USD': {
        simbol = '$';
        break;
      }
      case 'EUR': {
        simbol = '€';
        break;
      }
      default: {
        simbol = 'S/';
      }
    }
    return simbol;
  }

  clearSelections() {
    if (this.view === '2') {
      let checkboxes = this.shadowRoot.querySelectorAll('.choiceChk');
      this.resetCheckBoxes(checkboxes);
      this.enableAccepted = false;
    }
  }

  claimRegister() {
    let option = {
      id: '2',
      name: 'Harec - Registro de casos'
    };
    this.shadowRoot.querySelector('#modalTransferReversal').close();
    this.dispatch('list-options-fcc-detail', option);
  }

  selectItem() {
    this.clearSelections();
  }

  // Define a template
  render() {
    let _list = (this.isEmpty(this.extract(this.transactionsList, '0.id', '')) ? [] : this.transactionsList);
    return html`
      <style>${this.constructor.shadyStyles}</style>
      <div class='container' .hidden='${!this.visible}' title=''>
      <fieldset title=''>
        <legend>
          <h4 class='title'>${this.title}</h4>
        </legend>

        <div class='row' style="display:none;">
          <bbva-web-form-select .hidden="${this.view !== '1'}" label="Mes de proceso" id='cbbMonth' .items=${this.itemsMonth} @change=${this.selectItem}>
          </bbva-web-form-select>
        </div>  

        ${this.view == '1' ? html`
        <div class='template-content' title='' .hidden='${this.view !== '1'}' >
          <div class="row row--filter row--content-3 row--container has-center-v">
            <div>
              <bbva-form-date optional-label="" label="Desde" class="is-blue" id="dtFechaDesde" value="${this.dateFrom}"></bbva-form-date>
            </div>
            <div>
              <bbva-form-date optional-label="" label="Hasta" class="is-blue" id="dtFechaHasta" value="${this.dateTo}"></bbva-form-date>
            </div>
            <div class='center'>
              <cells-icon class='is-animate icon-size-24 is-cursor is-blue' icon='coronita:search' @click="${this.findTransactions}"></cells-icon>
            </div>
          </div>
        </div>
        <div class="row is-gapless is-1 list-movements${this.transactionsList.length === 0 ? '-empty' : ''} has-scroll" .hidden='${!this.visibleList}'>
          <div class="">
            ${this.transactionsList.length !== 0 ? html`
              ${_list.map(e => {
                return html` <div class="rRow is-cursor" id="row_${e.id}">
                  <div class="item">
                    <div class="item-checkbox">
                      <bbva-form-checkbox data-item="${JSON.stringify(e)}" name="checkBoxName" class="choiceChk" id="chk_${e.id}" value="${e.id}" @click="${this.clickChkDocument}" optional-label="  ">
                      </bbva-form-checkbox>
                    </div>
                  </div>
                  <div class="">
                    <time datetime="${this.setDateFomat(e.operationDate)}" class='time'>
                      <span class="day">${this.dateData.day}</span>
                      <abbr class="month">${this.dateData.month}.</abbr>
                      <span class="year">${this.dateData.year}</span>
                    </time>
                  </div>
                  <div class="item-column item-space">
                    <div class="item-space-wide">
                      <div class="item-space heading capitalize-uppercase">${e.concept}<br><small>Operación: ${this.extract(e, 'tk.IDTransaction', '--')}</small></div>
                      <div class="content-negative">
                        <span dateamount='${this.setAmountFormat(e.amount)}' class='${this.amountData.negative}'>
                          <span class='amount'>${this.amountData.number}</span>
                          <span class='fractional'>${this.amountData.decimal}</span>
                        </span>
                      </div>
                    </div>
                  </div>
                  `;
                })
              }` :  html`
              <div class="empty-content is-small message-size">
                Sin información para mostrar.
              </div>`}
          </div>
        </div>
        ${this.transactionsList.length !== 0 ? html`
        <div class="center" .hidden='${!this.visibleButton}'>
          <bbva-button-default .disabled="${!this.enableAccepted}" class="buttonAccept" @click=${this.acceptTransfer}>Aceptar</bbva-button-default>
        </div>
        ` : html``}
        ` : html``}
        ${this.view == '2' ? html`
          <div class='row is-rowless is-4'>
            <div class='column-1 label-size'><span class="item-space heading capitalize-uppercase"><b>NRO. DE OPERACIÓN:</b></span></div>
            <div class='column-2'>${this.dataOperation.number}</div>
            <div class='column-3 label-size '><span class="item-space heading capitalize-uppercase"><b>FECHA Y HORA DE OPERACIÓN:</b></span></div>
            <div>${this.dataOperation.dateHour}</div>
          </div>
          ${this.dataOperation.sendingProof !== '' ? html`
            <div class='row is-rowless is-4 has-center-v'>
              <div class='label-size column-1'><span class="item-space heading capitalize-uppercase"><b>ENVÍO DE CONSTANCIA:</b></span></div>
              <div class='row grid-40 is-gapless has-center-v'><cells-icon class='is-animate icon-size-24 is-blue' icon='coronita:email'></cells-icon> 
              <div>${this.dataOperation.sendingProof}</div></div>
            </div>
          `: html``}
          <div class='row'></div>
          <div class='row row--content-2 has-center-v color-body'>
            <div class='center'>
              <cells-icon class='is-animate icon-size-24 is-blue' icon='coronita:${this.dataSuccess.icon}'></cells-icon>
            </div>
            <div class='is-rowless'>
              ${this.dataSuccess.message}
            </div>
          </div>
          <div class='row has-center-v message-body'>
            <div>
              <span><b class='cursiva'>Confirma con el cliente</b></span><br>
            </div>
            <div></div>
          </div>
          <div class='row row--content-2 has-center-v message-body'>
            <div class='center'>
              <cells-icon class='is-animate icon-size-24' icon='coronita:${this.dataSuccess.iconToClient}'></cells-icon>
            </div>
            <div class=''>
              <div>
                <span class='span-body'>${this.dataSuccess.messageToClient} 
              </div>
            </div>
          </div>
        `: html``}

      </fieldset>
      <cells-modal-fcc width="600px"  @on-change-close=${this.clearSelections} id='modalOriginAccount' title="MOVIMIENTOS SELECCIONADOS (${this.selectionsMovements.length})">
        <div class='container-modal' title=''>

         ${this.selectionsMovements.map(movement => html`
         <div class="content-item-selection" >
              <div class="concept">${movement.concept}</div>
              <div class="amount" >${movement.amountValue}</div>
          </div>   
         `)}
          <div class='row row--content-1 has-center-v message-body'>
            <div class='center'>
              <bbva-button-default id="btn-confirmar" class="buttonConfirm" @click=${this.confirmTransfer}>${this.simulationOk ? 'Abonar' : 'Confirmar movimientos'}</bbva-button-default>
              <bbva-button-default class="buttonCancel" @click="${this.cancelConfirm}" >cancelar</bbva-button-default>
            </div>
          </div>
        </div>
      </cells-modal-fcc>
      <cells-modal-fcc width='600px' id='modalTransferReversal' title='ATENCIÓN POR FRAUDE'>
        <div class='container-modal' title=''>
          <div class='row row--content-2 has-center-v color-body contet-info-s2'>
            <div class='center'>
              <cells-icon class='is-animate icon-size-24 is-blue' icon='coronita:${this.dataModal.icon}'></cells-icon>
            </div>
            <div class='is-rowless'>
              ${this.dataModal.message}
            </div>
          </div>
          <div class='row has-center-v message-body'>
            <div>
              <span><b class='cursiva'>Confirma con el cliente</b></span><br>
            </div>
            <div></div>
          </div>
          <div class='row row--content-2 has-center-v message-body contet-speech-s2'>
            <div class='center'>
              <cells-icon class='is-animate icon-size-24' icon='coronita:${this.dataModal.iconToClient}'></cells-icon>
            </div>
            <div class=''>
              <div>
                <span class='span-body'>${this.dataModal.messageToClient} 
              </div>
            </div>
          </div>
          <div class="content-buttons">
              <bbva-button-default class="buttonConfirm" @click="${() => this.shadowRoot.querySelector('#modalTransferReversal').close() }">Aceptar</bbva-button-default>
            </div>
          ${this.status == '1' ? html`
          <div class='row row--content-1 has-center-v message-body'>
            <div class='center-div'>
              <bbva-button-default class="buttonContinue" @click=${this.continueTransfer}>Continuar</bbva-button-default>
            </div>
          </div>
          ` : html``}
          ${this.status == '2' ? html`
          <div class='row row--content-1 has-center-v message-body'>
            <div class='center-div'>
              <bbva-button-default ?hidden="${true}" class="buttonContinue register-case" @click=${this.claimRegister}>Registrar caso</bbva-button-default>
              <bbva-button-default class="buttonContinue" @click=${()=>this.shadowRoot.querySelector('#modalTransferReversal').close()}>Cerrar</bbva-button-default>
            </div>
          </div>
          ` : html``}
        </div>
      </cells-modal-fcc>
    `;
  }
}